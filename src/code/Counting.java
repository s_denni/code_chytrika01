/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package code;

import code.databases.Barang;
import code.databases.Koneksidb;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author S_Denni
 */
public class Counting {
    java.util.Date tanggal = new java.util.Date();
    private Koneksidb koneksi = new Koneksidb();
    Calendar cal = Calendar.getInstance();
    int tahun;
    int bulan;
    int hari;
    
    public void _setTanggal(){
        cal.setTime(tanggal);
        
        tahun = cal.get(Calendar.YEAR);
        bulan = cal.get(Calendar.MONTH);
        hari = cal.get(Calendar.DATE);
        
//        System.out.println(tanggal);
//        System.out.println("thn : "+tahun+" | bulan : "+bulan+" | hari : "+hari);
    }

    public String tahun(){
        _setTanggal();
        return String.valueOf(this.tahun);
    }
    
    public String bulan(){
        _setTanggal();
        String value = String.valueOf(this.bulan);
        if (value.length() < 2) {
            value = "0"+value;
        } else if (value.length() <3) {
            value = ""+value;
        }
        return value;
    }
    
    public String hari(){
        _setTanggal();
        String value = String.valueOf(this.hari);
//        System.out.println(value);
//        System.out.println(value.length());
        if (value.length() < 2) {
            value = "0"+value;
        } else if (value.length() <3) {
            value = ""+value;
        }
        return value;
    }
    
    public void _onCheck(){
        if(!Check()){
            _Default();
            System.out.println("setDefault Success");
        } else {
            
        }
    }
    
    public boolean Check(){
        boolean bool = false;
        int count = 0;
        
        ResultSet rs = null;
        Statement st;
        
        String sqlquery = "SELECT count(primaryRow) FROM counting";
        try {
            st = koneksi.Koneksidb().createStatement();
            rs = st.executeQuery(sqlquery);
            
            if(rs.first()){
                count = rs.getInt(1);
                
                if(count > 0){
                    bool = true;
                } else if( count == 0) {
                    bool = false;
                }
            }
          
            
        } catch (SQLException e) {
            System.out.println(e);
        }
        
        
//        System.out.println(" "+count);
        return bool;
    }
    
    public void _Default() {
        
        int defaultData = 0;
        try {
            
            String query = "INSERT INTO counting(jenis_count, jenisKey, merk_count, merkKey, batasan_count, batasanKey) "
                    + "VALUES (?,?,?,?,?,?)";
            PreparedStatement prepare = koneksi.Koneksidb().prepareStatement(query);
            
            prepare.setInt(1, defaultData);
            prepare.setInt(2, defaultData);
            prepare.setInt(3, defaultData);
            prepare.setInt(4, defaultData);
            prepare.setInt(5, defaultData);
            prepare.setInt(6, defaultData);
            
            koneksi.Insert(prepare);
            
        } catch (SQLException ex) {
            Logger.getLogger(Barang.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public ResultSet GetCounting() {
        ResultSet rs = null;
        Statement st;
        
        String sqlquery = "SELECT * FROM counting where primaryRow = 1";
        
        try {
            st = koneksi.Koneksidb().createStatement();
            rs = st.executeQuery(sqlquery);

        } catch (SQLException e) {
            System.out.println(e);
        }
        
        return rs;
    }
    
    public void _Update(String field, Object value) {
        try {
            
            String query = "UPDATE counting SET "+field+" = ? where primaryRow = 1";
//            System.out.print(query+ " " +value);
//            System.err.println("");
            
            PreparedStatement prepare = koneksi.Koneksidb().prepareStatement(query);
            
            prepare.setObject(1, value);
            
            koneksi.Insert(prepare);
            
        } catch (SQLException ex) {
            Logger.getLogger(Barang.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void diamond(){
        GetCounting();
    }
    
//    public ResultSet Get(String[] field) {
//        ResultSet rs = null;
//        Statement st;
//        
//        String getField = "";
//        for (int i = 0; i < field.length; i++) {
//            if(i != field.length){
//                getField = ""+getField + "" +field[i]+",";
//            } else {
//                getField = getField + field[i] +"";
//            }
//        }
//        
//        String sqlquery = "SELECT "+getField+" FROM counting where primaryRow = 1";
//        
//        try {
//            st = koneksi.Koneksidb().createStatement();
//            rs = st.executeQuery(sqlquery);
//            
//            if (rs.first()) {
//                
//            }
//
//        } catch (SQLException e) {
//            System.out.println(e);
//        }
//        
//        return rs;
//    }
    
    public static void main(String[] args) {
//        Counting count = new Counting();
//        
//        count._onCheck();
//        System.out.print(count.hari());
    }
}
