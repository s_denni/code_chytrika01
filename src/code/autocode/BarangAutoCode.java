/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package code.autocode;

import code.Counting;
import code.localsetting.AutoCode;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author S_Denni
 */
public class BarangAutoCode {
    private Counting counting = new Counting();
    
    String key = "B";
    
    public String UniqueCode(boolean reset){
        String value = "0001";
        int count = 0;
        
        ResultSet res = counting.GetCounting();
        try {
            if(res.first()){
                if(reset){
                    count = 1;
                } else {
                    count = res.getInt("barang_count") + 1;
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(AutoCode.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        if(count < 10) {
            value = "000"+count;
        } else if(count < 100){
            value = "00"+count;
        } else if (count < 1000){
            value = "0"+count;
        } else if (count < 10000){
            value = ""+count;
        }
        
        return value;
    }
    
    public boolean Reset(){
        boolean bool = false;
        String keyBarang = "";
        
        ResultSet res = counting.GetCounting();
        try {
            if(res.first()){
                keyBarang = res.getString("barangKey");
            }
        } catch (SQLException ex) {
            Logger.getLogger(AutoCode.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        if(!SubMain().equals(keyBarang)){
            bool = true;
        }
        
        return bool;
    }
    
    public String SubMain(){
        String value = "";
        String tahunKey = counting.tahun().substring(2, 4);
        String bulanKey = counting.bulan();
        value = bulanKey+tahunKey;
        return value;
    }
    
    public String Main(){
        String autoCode = "";
        String uniqueKey = this.key;
        String uniqueCode = UniqueCode(Reset());
        autoCode = uniqueKey+SubMain()+uniqueCode;
        return autoCode;
    }
    
    public void Update(String uniqueCode){
        
        String countString = uniqueCode.substring(5,9);
        String keyString = uniqueCode.substring(1,5);
        
        System.out.println(countString +" "+keyString);
        
        int count = Integer.parseInt(countString);
        String key= keyString;
        counting._Update("barang_count", count);
        counting._Update("barangKey", key);
    }
    
//    public static void main(String[] args) {
////        BarangAutoCode bac = new BarangAutoCode();
////        System.out.print(bac.Reset());
////        System.out.println(bac.Main());
////        bac.Update(bac.Main());
//    }
}
