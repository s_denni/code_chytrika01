/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package code.databases;

import code.Sejarah;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author S_Denni
 */
public class Sejarah_Pengembalian implements Sejarah{
    private Koneksidb koneksi = new Koneksidb();
    
    String idTrxRtr;
    String idUser;
    String namaUser;
    Date tanggal;
    String keterangan;
    String idTrxRetur_brg;
    String idTrxJual_brg;
    String idBrg;
    String namaBrg;
    int jml;

    public String getIdTrxRtr() {
        return idTrxRtr;
    }

    public void setIdTrxRtr(String idTrxRtr) {
        this.idTrxRtr = idTrxRtr;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String getNamaUser() {
        return namaUser;
    }

    public void setNamaUser(String namaUser) {
        this.namaUser = namaUser;
    }

    public Date getTanggal() {
        return tanggal;
    }

    public void setTanggal(Date tanggal) {
        this.tanggal = tanggal;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public String getIdTrxJual_brg() {
        return idTrxJual_brg;
    }

    public void setIdTrxJual_brg(String idTrxJual_brg) {
        this.idTrxJual_brg = idTrxJual_brg;
    }

    public String getIdBrg() {
        return idBrg;
    }

    public void setIdBrg(String idBrg) {
        this.idBrg = idBrg;
    }

    public String getNamaBrg() {
        return namaBrg;
    }

    public void setNamaBrg(String namaBrg) {
        this.namaBrg = namaBrg;
    }

    public int getJml() {
        return jml;
    }

    public void setJml(int jml) {
        this.jml = jml;
    }

    public String getIdTrxRetur_brg() {
        return idTrxRetur_brg;
    }

    public void setIdTrxRetur_brg(String idTrxRetur_brg) {
        this.idTrxRetur_brg = idTrxRetur_brg;
    }

    @Override
    public void Create() {
        try {
            
            String query = "INSERT INTO sejarah_pengembalian (idTrxRtr, idUser, namaUser, tanggal, keterangan, idTrxRetur_brg, idTrxJual_brg, idBrg, namaBrg, jml) "
                    + "VALUES (?,?,?,?,?,?,?,?,?,?)";
            PreparedStatement prepare = koneksi.Koneksidb().prepareStatement(query);
            
            prepare.setString(1, this.idTrxRtr);
            prepare.setString(2, this.idUser);
            prepare.setString(3, this.namaUser);
            prepare.setDate(4, this.tanggal);
            prepare.setString(5, this.keterangan);
            prepare.setString(6, this.idTrxRetur_brg);
            prepare.setString(7, this.idTrxJual_brg);
            prepare.setString(8, this.idBrg);
            prepare.setString(9, this.namaBrg);
            prepare.setInt(10, this.jml);
            
            koneksi.Insert(prepare);
            
        } catch (SQLException ex) {
            Logger.getLogger(Barang.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public ResultSet Show() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public ResultSet ShowGrouped() {
        ResultSet rs = null;
        Statement st;
        String sqlquery = "SELECT * FROM sejarah_pengembalian GROUP BY idTrxRtr";
        
        try {
            st = koneksi.Koneksidb().createStatement();
            rs = st.executeQuery(sqlquery);
        } catch (SQLException e) {
            System.out.println(e);
        }
        
        return rs;
    }

    @Override
    public ResultSet ShowByGroup(String idGroup) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ResultSet Show(Date date) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ResultSet Show(Date date1, Date date2) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
}
