/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.awt.Dimension;
import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;

/**
 *
 * @author S_Denni
 */
public class JinternalControl {
    public void addJinternalTo(JDesktopPane desktopPane,JInternalFrame internalFrame){
        desktopPane.add(internalFrame);
    }
    
    public void visibleInternal(JInternalFrame internalFrame){
        internalFrame.setVisible(true);
    }
    
    public void ShowJinternal(JDesktopPane desktopPane,JInternalFrame internalFrame){
        Dimension desktopSize = desktopPane.getSize();
            Dimension jInternalFrameSize = internalFrame.getSize();
            internalFrame.setVisible(true);
            
            //maximize size
            internalFrame.setSize(internalFrame.getMaximumSize());
            internalFrame.pack();
            internalFrame.setMaximizable(true);            
            internalFrame.setClosable(true);  
            internalFrame.setLocation((desktopSize.width - jInternalFrameSize.width)/2,
            (desktopSize.height- jInternalFrameSize.height)/2);
            
            desktopPane.add(internalFrame);
            internalFrame.toFront();
    }
    
    public void DisposeJinternalFrame(JDesktopPane desktopPane, JInternalFrame internalFrame){
//    public void DisposeJinternalFrame(JInternalFrame internalFrame){
        if(internalFrame != null){
            internalFrame.dispose();
            desktopPane.remove(internalFrame);
        }
    }
}